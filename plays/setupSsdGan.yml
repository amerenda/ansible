---
- hosts: gandalf
  sudo_user: root
  tasks:

  - name: check for new ssds
    fail: msg="System {{ inventory_hostname }} is missing sdn!"
    when: ansible_devices.sdn is not defined

  - name: check for new ssds
    fail: msg="System {{ inventory_hostname }} is missing sdo!"
    when: ansible_devices.sdo is not defined

  - name: create gpt disk label
    command: /sbin/parted -s /dev/{{ item }} mklabel gpt
    with_items: 
      - sdn
      - sdo
    ignore_errors: yes
    when: ansible_devices.sdn and ansible_devices.sdo is defined 

  - name: create /tmp partition
    command: /sbin/parted -s -a optimal /dev/{{ item }} mkpart primary 63s 167772223s
    with_items: 
      - sdn
      - sdo
    ignore_errors: yes
    
  - name: create /data/{13,14} partitions
    command: /sbin/parted -s -a optimal /dev/{{ item }} mkpart primary 167772224s 1761607743s 
    with_items:
      - sdn
      - sdo
    ignore_errors: yes

  - name: create raid array
    shell: "{{ item }}"
    with_items:
      - mdadm --create -R /dev/md0 --level=1 --raid-devices=2 /dev/sdn1 /dev/sdo1
    ignore_errors: yes

  - name: create vg
    lvg: vg=vg01 pvs=/dev/md0 state=present
    ignore_errors: yes

  - name: create lvm for tmp
    lvol: vg=vg01 lv=TMP size=64g
    ignore_errors: yes

  - name: create lvm for swap
    lvol: vg=vg01 lv=SWAP size=15g
    ignore_errors: yes

  - name: create swap
    shell: mkswap /dev/vg01/SWAP
    ignore_errors: yes

  - name: create /tmp filesystem
    filesystem: fstype=ext4 dev=/dev/vg01/TMP opts="-i 65536"

  - name: create /data/{13,14} filesystems
    filesystem: fstype=ext4 dev={{ item }} opts="-i 1048576 -O extent"
    with_items:
      - /dev/sdn2
      - /dev/sdo2

  - name: create /data/{11,12} filesystems
    filesystem: fstype=ext4 dev={{ item }} opts="-i 2097152 -m0 -O extent"
    with_items:
      - /dev/sdl
      - /dev/sdm

  - name: create mount points
    file: path={{ item }} state=directory owner=root group=root mode=755
    with_items:
      - /data/13
      - /data/14
      - /mnt/tmp

  - name: mount /dev/vg01/TMP to temp directory
    command: /bin/mount /dev/vg01/TMP /mnt/tmp
    ignore_errors: yes    
 
  - name: copy /tmp to /dev/vg01/TMP temp directory
    synchronize: src=/tmp/ dest=/mnt/tmp
    ignore_errors: yes
    delegate_to: "{{ inventory_hostname }}"

  - name: erase /tmp
    command: rm -rf /tmp/*
    ignore_errors: yes    

  - name: remove old tmp mount
    shell: "{{ item }}"
    with_items:
      - sed -i '/vg00-TMP/d' /etc/fstab 

  - name: add mount points to fstab 
    mount: name={{ item.name }} src={{ item.src }} 
           state=present fstype=ext4
    with_items:
      - { name: '/tmp', src: '/dev/mapper/vg01-TMP' }
      - { name: '/data/13', src: '/dev/sdn2' }
      - { name: '/data/14', src: '/dev/sdo2' }
    ignore_errors: yes    

  - name: ensure /tmp is 777
    file: path=/tmp state=directory owner=root group=root mode=0777

  - name: reboot server
    command: /sbin/shutdown -r now "Ansible remounts /tmp"
    
  - name: waiting for sever to come back
    local_action: wait_for host={{ inventory_hostname }} state=started
    sudo: false

  - name: erase temporary files created
    file: path={{ item }} state=absent
    with_items:
      - /mnt/tmp
