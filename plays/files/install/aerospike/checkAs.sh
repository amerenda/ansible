#!/bin/bash
if [ -z "$*" ]; then
    echo "Please provide an adserver (e.g. ads.placeiq.com)"
fi

ADSERVER=$1
curl -s "http://${ADSERVER}/configuration?uid=84e63db8-6f9c-46a5-b405-ec693a60063c&prefix=aerospike.enricher&ATOKEN=adserverSecretlyProvidesManagmentEndpoints" | grep -A 14 'enrichment' | grep -o -E "10\.(1|16)\.70\.2[01]0" | head -n 1 2> /dev/null
