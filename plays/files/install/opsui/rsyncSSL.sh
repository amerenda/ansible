#!/bin/bash
# Script to securely backup ssl certificates on the placeIQ opsui servers
# Witten by Alex Merenda

HOST=$(hostname)
DTF=$(date +"%Y%m%d")
LOCAL_SUM=$(tar -P -cf - /etc/ssl/certs/ | md5sum | awk '{ print $1 }')
# shellcheck disable=SC2029
REMOTE_SUM=$(ssh rundeck@10.1.41.63 "cat /data1/backup/ssl_certs/${HOST}/md5-${HOST}.txt")
FILE=/home/rundeck/${HOST}-ssl-certs-${DTF}.tar.gz

echo "local sum : ${LOCAL_SUM}"
echo "remote sum: ${REMOTE_SUM}"

if [ "${LOCAL_SUM}" != "${REMOTE_SUM}" ]; then
	echo "Sums do not match, sending new file"
	echo "${LOCAL_SUM}" > /home/rundeck/md5-"${HOST}".txt
	tar czf "${FILE}" /etc/ssl/certs/
	gpg --trust-model always --encrypt --recipient 'placeiq_devops' "${FILE}"
	rsync -avr -e 'ssh' "${FILE}".gpg rundeck@10.1.41.63:/data1/backup/ssl_certs/"${HOST}"/
	rsync -avr -e 'ssh' /home/rundeck/md5-"${HOST}".txt rundeck@10.1.41.63:/data1/backup/ssl_certs/"${HOST}"
	rm -f "${FILE}".gpg
	rm -f "${FILE}"
	rm -f /home/rundeck/md5-"${HOST}".txt
else
	echo "Sums match, aborting"
fi
