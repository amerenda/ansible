#!/bin/bash
# This script runs on opsui-35.nym1 and syncs sites between opsui-35.nym1 and opsui-35.sec
# Written by Alex Merenda for PlaceIQ

#  Variables
SSH_USER='rundeck'
SERVER='opsui-35.sec.placeiq.net'
TARGET="${SSH_USER}@${SERVER}"
RSYNC_OPTS=(-avr -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null")
RSYNC_C_OPTS=(--dry-run -rvcLe "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null")
DEST_DIR='/etc/nginx/sites-available'
LOCAL_DIR='/etc/nginx/sites-available'
ENABLED_DIR='/etc/nginx/sites-enabled'
LOG_D='/var/log/rundeck'
LOG_F="${LOG_D}/rsyncSites.log"
SITES_ARRAY=()

function logger(){
    msg=$1
    dt=$(date +"%Y-%m-%d %H:%M:%S")
    echo "${dt} ${msg}" >> "${LOG_F}"
}

logger 'Comparing source and dest dirs'
for sites in $(/usr/bin/rsync "${RSYNC_C_OPTS[@]}" ${ENABLED_DIR}/* ${TARGET}:${DEST_DIR}) ; do
    if [[ ${sites} =~ .*\.net ]]; then
        SITES_ARRAY+=(${sites})
    fi
done


if [ "${#SITES_ARRAY[@]}" -eq 0 ]; then
    logger 'No new sites. Exiting'
    exit 0
fi

logger 'Starting rsync'
for sites in "${SITES_ARRAY[@]}" ; do
    /usr/bin/rsync "${RSYNC_OPTS[@]}" ${LOCAL_DIR}/${sites} ${TARGET}:${DEST_DIR} > /dev/null
    if [ $? -eq 0 ]; then
        logger "Syncing: ${LOCAL_DIR}/${sites} To: ${TARGET}:${DEST_DIR}"
    else
        logger "ERROR  while syncing: ${LOCAL_DIR}/${sites} To: ${TARGET}:${DEST_DIR}" 
    fi
done
logger 'Rsync finished'

logger 'Creating symlinks'
for sites in "${SITES_ARRAY[@]}" ; do
    /usr/bin/ssh ${TARGET}  sudo /bin/ln -s ${DEST_DIR}/${sites} ${ENABLED_DIR}/${sites} > /dev/null
    if [ $? -eq 0 ]; then
        logger "Linking ${DEST_DIR}/${sites} To: ${ENABLED_DIR}/${SITES}"
    else
        logger "ERROR while linking ${DEST_DIR}/${sites} To: ${ENABLED_DIR}/${SITES}" 
    fi
done
logger 'symlinks finished'

/usr/bin/ssh ${TARGET} sudo /etc/init.d/nginx reload > /dev/null
    if [ $? -eq 0 ]; then
        logger 'successfully reloaded nginx'
    else
        logger "ERROR error reloading nginx." 
    fi
