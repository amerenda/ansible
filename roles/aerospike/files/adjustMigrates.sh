#!/bin/bash
# Script to adjust Aerospike migrations to various presets
# Written by Alex Merenda for PlaceIQ
IP=0.0.0.0

DefaultMigrate() {
    IP=$1
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-threads=1;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-hwm=10;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-lwm=5;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-priority=40;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-sleep=500;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-max-num-incoming=256;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-priority=10;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-sleep=500;'" 
}

MaxProd() {
    IP="$1"
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-threads=5;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-hwm=100;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-lwm=50;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-priority=240;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-sleep=250;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-max-num-incoming=256;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-priority=240;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-sleep=250;'" 
}

MaxAero() {
    IP="$1"
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-threads=25;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-hwm=250;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-lwm=25;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-priority=0;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-xmit-sleep=0;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-max-num-incoming=256;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-priority=0;'" 
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-read-sleep=0;'" 

}

DisableMigrate() {
    IP="$1"
    /usr/bin/asmonitor -h "$IP" -e "asinfo -v 'set-config:context=service;migrate-threads=0;'" 
}


RETURN=true
until [ "$RETURN" == false ]
do
        echo "Please Select which cluster you would like to configure"
        echo "1) Cluster 1 (10.1.40.[200:204])"
        echo "2) Cluster 2 (10.1.40.[210:214])"
        echo "3) Cluster 3 (10.16.40.[200:204])"
        read -p "Enter a selection: " CLUSTER
        
        case "$CLUSTER" in
                1) IP=10.1.70.200
                    RETURN=false
                    ;;
                2) IP=10.1.70.210
                    RETURN=false
                    ;;
                3) IP=10.16.70.200
                    RETURN=false
                    ;;
                *) echo "Invalid Selection"
                    RETURN=true
                    clear
                    ;;
        esac
done

echo "***************************"
echo "You have selected Cluster $CLUSTER"
echo "***************************"

RETURN=true
until [ "$RETURN" == false ]
do
        echo "Please select what migration presets you would to apply to Cluster $CLUSTER"
        echo "1) Aerospike Defaults" 
        echo "2) Maximum production-safe speeds"
        echo "3) Maximum Aerospike-safe speeds (Not for use in prod)"
        echo "4) Disable Migrations"
        read -p "Enter a selection: " MIGRATE

        case "$MIGRATE" in
                1) DefaultMigrate "$IP"
                    RETURN=false
                    ;;
                2) MaxProd "$IP"
                    RETURN=false
                    ;;
                3) MaxAero "$IP"
                    RETURN=false
                    ;;
                4) DisableMigrate "$IP"
                    RETURN=false
                    ;;
                *) echo "Invalid Selection"
                    RETURN=true
                    clear
                    ;;
        esac
done 
