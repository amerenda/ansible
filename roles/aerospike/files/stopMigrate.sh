#!/bin/bash
# This script is designed to be run as a cron job every 5 minutes. 
# It will check if a node in the cluster is down,
# and if the high water mark (HWM) for memory has been breached.
# If it has, it will turn off aerospike migrations. 
# This is done to avoid the memory overflow that can happen during migrations
# at the (minor) risk of losing data.
# Written by Alex Merenda for PlaceIQ

cluster_size=5
aconfig=/etc/aerospike/aerospike.conf
namespace=()
hwm=()
mem=()
dtf=$(date +"%Y%m%d")
dt_t=$(date +"%T")
logd=/var/log/aerospike/
logf=$logd/stopmigrates-$dtf
breach=/opt/aerospike/breach-$dtf
cluster=$(hostname -f | cut -c11- | rev | hostname | cut -c11- | rev | cut -c21- | rev)


# Check if the cluster size is as expected. If it is, the script will exit.
if [ `asmonitor -e info | grep -a3 Cluster | awk {' print $3 '} | tail -n1` -eq $cluster_size ];
then
        exit $?
fi

# Get a list of namespace and their HWM, and current memory usage
for i in `grep namespace $aconfig | awk '{ print $2 }'`; do
        if [ $i = 'blacklist' ]; then
                :
        else
            namespace+=($i)
            hwm+=(`grep -a1 $i $aconfig | grep high-water-memory-pct | awk {' print $2 '}`)
            mem+=(`asmonitor -e "info Namespace" | grep $i | awk '{ print $12 }' | tail -n 1`)
        fi
done



# Check if the hwm is being breached
end=$((${#namespace[@]}-1))
for i in $(seq 0 "$end"); do
	if [ "${mem[$i]}" -ge "${hwm[$i]}" ]; then 
		 if [ ! -f $breach ]; then
			touch $logf
			touch $breach
			msg="$dt_t - An aerospike server is down in $cluster and the ${namespace[$i]} namespace has breached the memory high water mark! Stopping migrations" 
            echo -ne "$msg" | mailx -s "*CRITICAL* stop writes on Aerospike $cluster" devops@placeiq.com
			echo -ne "$msg" >> $logf 
			asmonitor -e "asinfo -v set-config:context=service;migrate-threads=0" 
		fi
	fi
done
