#!/bin/bash
#echo -n "$(date +%Y/%m/%d-%H:%M:%S)  " >> /var/log/nginx/nginxRPS.log
grep "$(date +%H:%M --date="30 second ago")" /var/log/nginx/ssl-traffic.log | wc -l | awk '{ avg = $1 / 60 } END { print avg }' >> /var/log/nginx/nginxRPS.log
